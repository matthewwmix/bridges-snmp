#!/usr/bin/python3

from easysnmp import Session
import datetime
from influxdb import InfluxDBClient
from apscheduler.schedulers.background import BlockingScheduler
import traceback

IPS = [
       "10.0.8.4",
       "10.0.7.68",
       "10.0.7.116",
       "10.0.8.44",
       "10.0.7.21",
       "10.0.7.37"
]

# Create an SNMP session to be used for all our requests

def getSnmpData(ip):
    session = Session(hostname=ip, community='bridges', version=2)

    hostname = session.get('sysName.0').value
    uptime = int(session.get('sysUpTimeInstance').value)

    interfaces = []
    for iface in session.walk('ifDescr'):
        iface_name = iface.value
        iface_adminStatus = session.get('ifAdminStatus.{}'.format(iface.oid_index)).value
        iface_status = session.get('ifOperStatus.{}'.format(iface.oid_index)).value
        inOctets = int(session.get('ifHCInOctets.{}'.format(iface.oid_index)).value)
        outOctets = int(session.get('ifHCOutOctets.{}'.format(iface.oid_index)).value)
        inUnicastPackets = int(session.get('ifHCInUcastPkts.{}'.format(iface.oid_index)).value)
        outUnicastPackets = int(session.get('ifHCOutUcastPkts.{}'.format(iface.oid_index)).value)
        inMulticastPackets = int(session.get('ifHCInMulticastPkts.{}'.format(iface.oid_index)).value)
        outMulticastPackets = int(session.get('ifHCOutMulticastPkts.{}'.format(iface.oid_index)).value)
        inBroadcastPackets = int(session.get('ifHCInBroadcastPkts.{}'.format(iface.oid_index)).value)
        outBroadcastPackets = int(session.get('ifHCOutBroadcastPkts.{}'.format(iface.oid_index)).value)

        inPackets = inUnicastPackets + inMulticastPackets + inBroadcastPackets
        outPackets = outUnicastPackets + outMulticastPackets + outBroadcastPackets

        interfaces.append({
            "name": iface_name,
            "status": iface_status,
            "adminStatus": iface_adminStatus,
            "inOctets": inOctets,
            "outOctets": outOctets,
            "inPackets": inPackets,
            "outPackets": outPackets,
            "inUnicastPackets": inUnicastPackets,
            "outUnicastPackets": outUnicastPackets,
            "inMulticastPackets": inMulticastPackets,
            "outMulticastPackets": outMulticastPackets,
            "inBroadcastPackets": inBroadcastPackets,
            "outBroadcastPackets": outBroadcastPackets
            })

    return hostname,uptime,interfaces

def getZuluTimeString():
    return "{}Z".format(datetime.datetime.utcnow().isoformat(sep=' ', timespec='milliseconds'))

def getInfluxMeasurement(name, tags, time, fields):
    return {
            "measurement": name,
            "tags": tags,
            "time": time,
            "fields": fields
            }

def getUptimeMeasurement(hostname, uptime):
    return getInfluxMeasurement("snmp_uptime",
            {
                "Producer": hostname
            },
            getZuluTimeString(),
            {
                "uptime": uptime
            })

def getInterfaceMeasurement(hostname, interfaces):
    return getInfluxMeasurement("snmp_interfaces",
            {
                "Producer": hostname
            },
            getZuluTimeString(),
            {
                "total": len(interfaces),
                "adminUp": len([i for i in interfaces if i["adminStatus"] == "1"]),
                "adminDown": len([i for i in interfaces if i["adminStatus"] == "2"]),
                "up": len([i for i in interfaces if i["status"] == "1"]),
                "down": len([i for i in interfaces if i["status"] == "2" and i["adminStatus"] != "2"])
            })

def getBytesMeasurement(hostname, interfaceName, bytesIn, bytesOut):
    return getInfluxMeasurement("snmp_bytes",
            {
                "Producer": hostname,
                "interface-name": interfaceName
            },
            getZuluTimeString(),
            {
                "bytesIn": bytesIn,
                "bytesOut": bytesOut
            })

def getPacketsMeasurement(hostname, interfaceName, inTotal, outTotal, inUnicast, outUnicast, inMulticast, outMulticast, inBroadcast, outBroadcast):
    return getInfluxMeasurement("snmp_packets",
            {
                "Producer": hostname,
                "interface-name": interfaceName
            },
            getZuluTimeString(),
            {
                "in": inTotal,
                "out": outTotal,
                "inUnicast": inUnicast,
                "outUnicast": outUnicast,
                "inMulticast": inMulticast,
                "outMulticast": outMulticast,
                "inBroadcast": inBroadcast,
                "outBroadcast": outBroadcast
            })

def writeToInflux(measurements):
    client = InfluxDBClient('localhost', 8086, 'admin', 'bridges', 'mydb')

    client.create_database("mydb")
    client.write_points(measurements)

def getAndStore(ip):
    try:
        measurements = []
        hostname,uptime,interfaces = getSnmpData(ip)
        measurements.append(getUptimeMeasurement(hostname, uptime))

        for interface in interfaces:
            measurements.append(getBytesMeasurement(hostname, interface["name"], interface["inOctets"], interface["outOctets"]))
            measurements.append(
                    getPacketsMeasurement(
                        hostname,
                        interface["name"],
                        interface["inPackets"],
                        interface["outPackets"],
                        interface["inUnicastPackets"],
                        interface["outUnicastPackets"],
                        interface["inMulticastPackets"],
                        interface["outMulticastPackets"],
                        interface["inBroadcastPackets"],
                        interface["outBroadcastPackets"])
                    )

        measurements.append(getInterfaceMeasurement(hostname, interfaces))

        writeToInflux(measurements)
    except Exception as ex:
        print("error connecting to ip: {}".format(ip))
        traceback.print_exc()
    finally:
        print("------------------------\n")


if __name__ == '__main__':
    print("Starting snmp poller.")
    scheduler = BlockingScheduler()
    
    for ip in IPS:
        scheduler.add_job(getAndStore, 'interval', minutes=5, kwargs={ "ip": ip })

    scheduler.start()
