FROM centos:7

RUN yum update -y && \
    yum install python3 \
                python3-pip \
                python3-devel \
                gcc \
                net-snmp-devel -y  && \
    pip3 install easysnmp && \
    pip3 install apscheduler && \
    pip3 install influxdb

COPY snmp.py /snmp.py

ENTRYPOINT ["/snmp.py"]
